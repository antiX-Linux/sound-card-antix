��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �      7  $   X     }     �     �     �  	   �     �  
        "     6     I  =   h                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>, 2016-2017
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 (Seçilen aygıta bağlanabildi) (Seçilen aygıta bağlanılamıyor) Şimdiki öntanımlı %s dir Ses kartı/aygıtı bulunamadı Sadece bir ses kartı bulundu Lütfen ses kartını seçin Çıkış Ses kartı %s'e ayarlı Ses deneme Deneme başarısız Deneme başarılı 6 saniyeye kadar ses deneniyor Sesin çalışıp çalışmadığını denemek ister misiniz? 