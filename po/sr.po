# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Лазар Миловановић <lazar.milovanovic@outlook.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-06-29 23:41-0600\n"
"PO-Revision-Date: 2015-06-29 21:26+0000\n"
"Last-Translator: Лазар Миловановић <lazar.milovanovic@outlook.com>, 2019\n"
"Language-Team: Serbian (http://www.transifex.com/anticapitalista/antix-development/language/sr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sr\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: bin/alsa-set-default-card:21 bin/alsa-set-default-card:33
msgid "No sound cards/devices were found"
msgstr "Звучне карте/уређаји нису пронађени"

#: bin/alsa-set-default-card:24
msgid "Only one sound card was found."
msgstr "Само једна звучна карта је пронађена"

#: bin/alsa-set-default-card:37
msgid "Current default is %s"
msgstr "Тренутно је подразумевано %s"

#: bin/alsa-set-default-card:40
msgid "Quit"
msgstr "Напусти"

#: bin/alsa-set-default-card:42
msgid "Please Select sound card"
msgstr "Изаберите звучну карту"

#: bin/alsa-set-default-card:51 bin/alsa-set-default-card:58
msgid "Sound card set to %s"
msgstr "Звучна карта је постављена на %s"

#: bin/alsa-set-default-card:56
msgid "Sound test"
msgstr "Провера звука"

#: bin/alsa-set-default-card:60
msgid "Would you like to test if the sound works?"
msgstr "Да ли бисте хтели да проверите исправност звука?"

#: bin/alsa-set-default-card:65
msgid "Testing sound for up to 6 seconds"
msgstr "Проверавам звук највише 6 секунди"

#: bin/alsa-set-default-card:72
msgid "Test succeeded"
msgstr "Провера је успешна"

#: bin/alsa-set-default-card:73
msgid "(Could connect to the selected device)"
msgstr "(Могао сам да се повежем на изабрани уређај)"

#: bin/alsa-set-default-card:78
msgid "Test failed"
msgstr "Провера је неуспешна"

#: bin/alsa-set-default-card:79
msgid "(Could not connect to the selected device)"
msgstr "(Нисам могао сам да се повежем на изабрани уређај)"
