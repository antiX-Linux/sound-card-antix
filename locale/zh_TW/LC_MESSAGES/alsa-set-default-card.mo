��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  $   6  $   [     �     �     �     �     �     �     �     	          &  *   =                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Shoichi Chou <shoichi.chou@gmail.com>, 2021
Language-Team: Chinese (Taiwan) (http://www.transifex.com/anticapitalista/antix-development/language/zh_TW/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_TW
Plural-Forms: nplurals=1; plural=0;
 （可以連接到選擇的裝置） （無法連接到選擇的裝置） 當前預設值為 %s 找不到音效卡/裝置 只找到一張音效卡。 請選擇音效卡 退出 音效卡設定為 %s 聲音測試 測試失敗 測試成功了 測試聲音長達6秒 您想測試一下聲音是否正常嗎？ 