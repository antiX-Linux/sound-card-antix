��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  0   l  2   �     �  )   �  *        C     b  "   i  
   �     �     �  "   �  1   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2017
Language-Team: Slovak (http://www.transifex.com/anticapitalista/antix-development/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 (Pripojenie k zvolenému zariadeniu sa podarilo) (Pripojenie k zvolenému zariadeniu sa nepodarilo) Momentálne predvolená je %s Neboli nájdené žiadne karty/zariadenia Bola rozpoznaná iba jedna zvuková karta. Prosím vyberte zvukovú kartu Koniec Zvuková karta je nastavená na %s Test zvuku Test zlyhal Test prebehol úspešne Testovanie zvuku po dobu 6 sekúnd Prajete si otestovať či zvuková karta funguje? 