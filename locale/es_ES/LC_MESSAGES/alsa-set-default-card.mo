��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  .   j  1   �     �  1   �  (     )   E     o  +   u     �     �     �  %   �  ,   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Roberto Saravia <saravia.jroberto@gmail.com>, 2015
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 (se pudo conectar al dispositivo seleccionado) (no se pudo conectar al dispositivo seleccionado) La predeterminada actual es %s No se encontraron tarjetas/dispositivos de sonido Solo se encontró una tarjeta de sonido. Por favor seleccione la tarjeta de sonido Salir La tarjeta de sonido está configurada a %s Prueba de sonido Prueba fallida La prueba fue exitosa Probando el sonido durante 6 segundos ¿Le gustaría probar si el sonido funciona? 