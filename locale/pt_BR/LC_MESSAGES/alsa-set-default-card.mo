��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  3   y  8   �     �  2   �  '   /  #   W     {     �     �     �     �      �  4   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020-2021
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 (Foi possível conectar ao dispositivo selecionado) (Não foi possível conectar ao dispositivo selecionado) O padrão atual é %s Não foram encontrados placas/dispositivos de som. Foi encontrada apenas uma placa de som. Por favor, selecione a placa de som Sair A placa de som definida foi %s Teste de som O teste falhou O teste foi bem-sucedido Teste de som por até 6 segundos Você gostaria de testar se o som está funcionando? 