��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  ,   �  ,   �     �  $   �          ;     P  !   W     y     �     �  ,   �  .   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: sonny nunag <sonnynunag@yahoo.com>, 2019
Language-Team: Filipino (Philippines) (http://www.transifex.com/anticapitalista/antix-development/language/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
 (Hindi maaring kumonekta sa napiling device) (Hindi maaring kumonekta sa napiling device) Ang kasalukuyang default ay %s Walang sound card o device na nakita Isang soundcard ang nakita. Mamili ng sound card Umalis Ang sound card ay nakatakda sa %s Subukin ang Sound Ang pagsubok ay di matagumpay Ang pagsubok ay tagumpay Sinusubok ang sound ng hanggang 6 na segundo Gusto mo bang subukin kung umaandar ang sound? 